package com.myfxtrading.demofxtrading.unit;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import com.worldfirst.demofxtrading.model.Order;
import com.worldfirst.demofxtrading.services.OrderService;

public class OrderFunctionalTest {
    private static OrderService orderService = new OrderService();

    @BeforeClass
    public static void setUp() throws Exception {
	Order order1 = new Order(1L, 1L, "GBP", "BID", "50");
	Order order2 = new Order(2L, 2L, "USD", "BID", "1.21");
	orderService.insertOrder(order1);
	orderService.insertOrder(order2);
    }

    @Test
    public void get_exchange_rate_test() {
	assertEquals(orderService.getExchangeRate("GBP-USD"), "1.2100");
    }

    @Test
    public void get_all_orders_test() {
	assertEquals(orderService.findAll().size(), 2);
    }
    
    @Test
    public void get_order_by_id_test() {
	assertEquals(orderService.findOrderById(1L).get().getId().intValue(),1);
    }


    @Test
    public void modify_order_by_id_test() {
	Order order1 = new Order(3L, 1L, "GBP", "BID", "20");
	orderService.insertOrder(order1);
	Order order2 = new Order(3L, 1L, "EUR", "BID", "20");
	Order newOrder = orderService.modify(order2);
	assertEquals(newOrder.getCurrency(),"EUR");
	orderService.cancelOrder(newOrder.getId());
    }

    
    @Test
    public void cancel_order_test() {
	Order order = new Order(3L, 3L, "EUR", "BID", "50");
	OrderService orderService = new OrderService();
	orderService.insertOrder(order);
	assertEquals(orderService.findAll().size(), 3);
	orderService.cancelOrder(order.getId());
	assertEquals(orderService.findAll().size(), 2);
    }

    @Test
    public void get_unmatched_orders_when_no_matching_test() {
	List<Order> unMatchedOrders = orderService.getUnMatchedOrders();
	assertEquals(unMatchedOrders.size(), 2);
    }
    
    @Test
    public void insert_one_matching_order_test() {
	Order order = new Order(3L, 3L, "GBP", "BID", "1");
	OrderService orderService = new OrderService();
	orderService.insertOrder(order);
	assertEquals(orderService.getMatchedOrders().size(), 2);
	orderService.cancelOrder(order.getId());
    }

    
    @Test
    public void insert_two_matching_orders_test() {
	Order order1 = new Order(3L, 3L, "GBP", "BID", "1");
	Order order2 = new Order(4L, 4L, "GBP", "ASK", "2");
	Order order3 = new Order(5L, 5L, "USD", "ASK", "2.42");

	OrderService orderService = new OrderService();
	orderService.insertOrder(order1);
	orderService.insertOrder(order2);
	orderService.insertOrder(order3);

	assertEquals(orderService.getMatchedOrders().size(), 4);
	
	orderService.cancelOrder(order1.getId());
	orderService.cancelOrder(order2.getId());
	orderService.cancelOrder(order3.getId());

    }

    
    @Test
    public void match_after_delete_order_test() {
	Order order = new Order(3L, 3L, "GBP", "BID", "1");

	OrderService orderService = new OrderService();
	orderService.insertOrder(order);
	assertEquals(orderService.getMatchedOrders().size(), 2);
	
	orderService.cancelOrder(order.getId());
	assertEquals(orderService.getMatchedOrders().size(), 0);

    }

}
