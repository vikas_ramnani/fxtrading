package com.myfxtrading.demofxtrading.endpoint;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import java.math.BigDecimal;

import org.junit.BeforeClass;
import org.junit.Test;

import com.worldfirst.demofxtrading.model.Order;

public class OrderTest {

    @BeforeClass
    public static void setUp() throws Exception {
	/*
	 * Order order1 = new Order(1L, 1L, "GBP", "BID", "50"); Order order2 = new
	 * Order(2L, 2L, "USD", "BID", "1.21"); Order order3 = new Order(3L, 2L, "GBP",
	 * "BID", "1"); Order order4 = new Order(4L, 2L, "USD", "ASK", "5000");
	 * 
	 * orderDao.insertOrder(order1); orderDao.insertOrder(order2);
	 * orderDao.insertOrder(order3); orderDao.insertOrder(order4);
	 */

    }

    @Test
    public void get_all_orders_test() {
	given().when().get("/fxtrading/orders?query=all").then().statusCode(200);
    }

    @Test
    public void get_order_by_id_test() {
	given().when().get("/fxtrading/orders/1").then().statusCode(200);
    }

    @Test
    public void modify_order() {
	Order requestOrder = new Order();
	requestOrder.setAmount(new BigDecimal(500));
	requestOrder.setUserId(2L);
	requestOrder.setCurrency("USD");
	requestOrder.setOrderType("BAD");

	given().contentType("application/json").body(requestOrder).when().put("/fxtrading/orders/1").then().body("amount", equalTo(500)).body("userId", equalTo(2)).body("currency", equalTo("USD")).body("orderType", equalTo("BAD")).statusCode(200);

	// given().when().get("/orders/1").then().body("orderType", equalTo("BAD"));
    }

    @Test
    public void create_order_test() {
	Order requestOrder = new Order();
	requestOrder.setAmount(new BigDecimal(500));
	requestOrder.setUserId(2L);
	requestOrder.setCurrency("USD");
	requestOrder.setOrderType("BID");

	given().contentType("application/json").body(requestOrder).when().post("/fxtrading/orders/").then().body("amount", equalTo(500)).body("userId", equalTo(2)).body("currency", equalTo("USD")).body("orderType", equalTo("BID")).statusCode(201);
    }

    @Test
    public void delete_order_test() {
	given().when().delete("/fxtrading/orders/4").then().statusCode(202);
    }

    @Test
    public void get_unmatched_orders_test() {
	given().when().get("/fxtrading/orders?query=unmatched").then().statusCode(200);
    }

    @Test
    public void get_matched_orders_test() {
	given().when().get("/fxtrading/orders?query=matched").then().statusCode(200);
    }

    @Test
    public void get_matched_orders_pair_test() {
	given().when().get("/fxtrading/orders/pairs/2").then().statusCode(200);
    }

}
