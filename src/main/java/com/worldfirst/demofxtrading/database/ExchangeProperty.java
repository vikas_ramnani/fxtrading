package com.worldfirst.demofxtrading.database;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.springframework.core.io.ClassPathResource;

public class ExchangeProperty {

    private static ExchangeProperty instance;
    private static Properties prop = new Properties();


    private ExchangeProperty() {
	InputStream input = null;
	try {
	    ClassPathResource classPathResource = new ClassPathResource("exchange.properties");
	    InputStream inputStream = classPathResource.getInputStream();

	    if (inputStream != null)
		input = inputStream;
	    else
		input = new FileInputStream("/Users/vikasramnani/Documents/workspace/DemoFxTrading/src/main/resources/exchange.properties");
	    prop.load(input);
	} catch (IOException ex) {
	    ex.printStackTrace();
	} finally {
	    if (input != null) {
		try {
		    input.close();
		} catch (IOException e) {
		    e.printStackTrace();
		}
	    }
	}
    }

    public static ExchangeProperty getInstance() {
	if (instance != null) {
	    return instance;
	} else {
	    return new ExchangeProperty();
	}
    }

    public String getProperty(String key) {
	return prop.getProperty(key);
    }

}