package com.worldfirst.demofxtrading.database.daoImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.worldfirst.demofxtrading.database.dao.OrderDao;
import com.worldfirst.demofxtrading.model.Order;

@Component
public class OrderDaoImpl implements OrderDao{
    
    private static Map<Long, Order> orders=new HashMap<Long, Order>();
	private static long incrementalAccountIds=0;

	/*static {
		++incrementalAccountIds;
		orders.put(incrementalAccountIds, new Order(incrementalAccountIds,1L,"GBP","BID","50", false));

		++incrementalAccountIds;
		orders.put(incrementalAccountIds, new Order(incrementalAccountIds,2L,"USD","BID","1.21", true));

		++incrementalAccountIds;
		orders.put(incrementalAccountIds, new Order(incrementalAccountIds,3L,"GBP","BID","1", true));

		++incrementalAccountIds;
		orders.put(incrementalAccountIds, new Order(incrementalAccountIds,4L,"GBP","ASK","5000", false));
	}*/
	
	public OrderDaoImpl(){

	}

    @Override
    public ArrayList<Order> getAllOrders() {
	return new ArrayList<Order>(orders.values());
    }

    @Override
    public Optional<Order> getOrderByid(Long id) {
	return Optional.ofNullable(orders.get(id));
    }

    @Override
    public Order insertOrder(Order order) {
	++incrementalAccountIds;
	order.setId(incrementalAccountIds);
	orders.put(incrementalAccountIds, order);
	return order;
    }

    @Override
    public Order modifyOrder(Order order) {
	return orders.put(order.getId(), order);
    }

    @Override
    public Order cancelOrder(Long id) {
	return orders.remove(id);
    }

    @Override
    public List<Order> getUnMatchedOrders() {
	List<Order> unMatchedOrders = new ArrayList<Order>();
	for(Order order : orders.values()) {
	    if(!order.isMatched()) {
		unMatchedOrders.add(order);
	    }
	}
	return unMatchedOrders;
    }

    @Override
    public List<Order> getMatchedOrders() {
	List<Order> matchedOrders = new ArrayList<Order>();
	for(Order order : orders.values()) {
	    if(order.isMatched()) {
		matchedOrders.add(order);
	    }
	}
	return matchedOrders;
    }

    @Override
    public List<Order> findAllOrderByOrderAndByCurrency(String orderType, String currency) {
	List<Order> filteredOrders = new ArrayList<Order>();
	for(Order order : orders.values()) {
	    if(order.getOrderType().equals(orderType) && !order.getCurrency().equals(currency)) {
		filteredOrders.add(order);
	    }
	}
	return filteredOrders;
    }

}
