package com.worldfirst.demofxtrading.database.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.worldfirst.demofxtrading.model.Order;

@Component
public interface OrderDao {
    public ArrayList<Order> getAllOrders();

    public Optional<Order> getOrderByid(Long id);

    public Order insertOrder(Order order);

    public Order modifyOrder(Order order);

    public Order cancelOrder(Long id);

    public List<Order> getUnMatchedOrders();

    public List<Order> getMatchedOrders();

    public List<Order> findAllOrderByOrderAndByCurrency(String orderType, String currency);
    

}
