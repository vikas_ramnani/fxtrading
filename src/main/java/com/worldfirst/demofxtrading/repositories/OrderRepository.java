package com.worldfirst.demofxtrading.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.worldfirst.demofxtrading.database.ExchangeProperty;
import com.worldfirst.demofxtrading.database.dao.OrderDao;
import com.worldfirst.demofxtrading.database.daoImpl.OrderDaoImpl;
import com.worldfirst.demofxtrading.model.Order;

@Component
public class OrderRepository {

    OrderDao orderDao = new OrderDaoImpl();
    
    public List<Order> findAll() {
	return orderDao.getAllOrders();
    }
    public Optional<Order> findOrderById(Long id) {
	return orderDao.getOrderByid(id);
    }
    public Order insertOrder(Order order) {
	return orderDao.insertOrder(order);
    }
    public Order modify(Order order) {
	return orderDao.modifyOrder(order);	
    }
    public Order cancelOrder(Long id) {
	return orderDao.cancelOrder(id);
    }
    public List<Order> getUnMatchedOrders() {
	return orderDao.getUnMatchedOrders();
    }
    public String getExchangeRate(String key) {
	return ExchangeProperty.getInstance().getProperty(key);
    }
    public List<Order> getMatchedOrders() {
	return orderDao.getMatchedOrders();
    }
    public List<Order> findAllOrderByOrderAndByCurrency(String orderType, String currency) {
	return orderDao.findAllOrderByOrderAndByCurrency(orderType, currency);
    }

}
