package com.worldfirst.demofxtrading.exceptions;

public class ResourceNotFoundException extends RuntimeException {


    private static final long serialVersionUID = 1L;
    private String resourceName;
    private String resourceField;
    private Long resourceFieldValue;

    public ResourceNotFoundException(String resourceName, String resourceField, Long resourceFieldValue) {
	super(resourceName + " with " + resourceField + " not found : " + resourceFieldValue);

	this.resourceName = resourceName;
	this.resourceFieldValue = resourceFieldValue;
	this.resourceField = resourceField;

    }

    /**
     * @return the resourceName
     */
    public String getResourceName() {
	return resourceName;
    }

    /**
     * @param resourceName
     *            the resourceName to set
     */
    public void setResourceName(String resourceName) {
	this.resourceName = resourceName;
    }

    /**
     * @return the resourceField
     */
    public String getResourceField() {
	return resourceField;
    }

    /**
     * @param resourceField
     *            the resourceField to set
     */
    public void setResourceField(String resourceField) {
	this.resourceField = resourceField;
    }

    /**
     * @return the resourceFieldValue
     */
    public Long getResourceFieldValue() {
	return resourceFieldValue;
    }

    /**
     * @param resourceFieldValue
     *            the resourceFieldValue to set
     */
    public void setResourceFieldValue(Long resourceFieldValue) {
	this.resourceFieldValue = resourceFieldValue;
    }

}
