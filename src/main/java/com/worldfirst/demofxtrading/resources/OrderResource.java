package com.worldfirst.demofxtrading.resources;

import java.math.BigDecimal;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.worldfirst.demofxtrading.model.Order;

public class OrderResource extends ResourceSupport{

    private Long id;
    private Long userId;
    private String orderType;
    private String currency;
    private BigDecimal amount;
    private boolean isMatched;
    
    public OrderResource() {
	
    }
    public OrderResource(Order order) {
	this.id=order.getId();
	this.userId=order.getUserId();
	this.orderType=order.getOrderType();
	this.amount=order.getAmount();
	this.currency=order.getCurrency();
	this.isMatched=order.isMatched();
    }
    
    @JsonProperty("id")
    public Long getResourceId() {
	return id;
    }
    
    public Long getUserId() {
	return userId;
    }
    
    public String getOrderType() {
	return orderType;
    }
    
    public String getCurrency() {
	return currency;
    }
    
    public BigDecimal getAmount() {
	return amount;
    }
    
    public boolean isMatched() {
	return isMatched;
    }

}
