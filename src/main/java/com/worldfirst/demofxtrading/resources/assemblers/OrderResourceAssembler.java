package com.worldfirst.demofxtrading.resources.assemblers;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import com.worldfirst.demofxtrading.controllers.OrderController;
import com.worldfirst.demofxtrading.model.Order;
import com.worldfirst.demofxtrading.resources.OrderResource;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

@Component
public class OrderResourceAssembler extends ResourceAssemblerSupport<Order, OrderResource> implements ResourceAssembler<Order, OrderResource> {

    public OrderResourceAssembler() {
	super(OrderController.class, OrderResource.class);
    }


    @Override
    public OrderResource toResource(Order order) {
	OrderResource orderResource = new OrderResource(order);
	orderResource.add(linkTo(OrderController.class).slash(order.getId()).withSelfRel());
	orderResource.add(linkTo(methodOn(OrderController.class).cacnelOrder(order.getId())).withRel("cancel"));
        return orderResource;
    }
}
