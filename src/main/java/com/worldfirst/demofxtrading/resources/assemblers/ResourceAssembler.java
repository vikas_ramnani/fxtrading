package com.worldfirst.demofxtrading.resources.assemblers;

import java.util.Collection;
import java.util.stream.Collectors;

public interface ResourceAssembler<Model, ModelResource> {
    public abstract ModelResource toResource(Model model);
    
    public default Collection<ModelResource> toResourceCollection(Collection<Model> models){
	return models.stream().map(m -> toResource(m)).collect(Collectors.toList());
    }
    

}
