package com.worldfirst.demofxtrading.controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.worldfirst.demofxtrading.exceptions.ResourceNotFoundException;
import com.worldfirst.demofxtrading.model.Order;
import com.worldfirst.demofxtrading.resources.OrderResource;
import com.worldfirst.demofxtrading.resources.assemblers.OrderResourceAssembler;
import com.worldfirst.demofxtrading.services.OrderService;

@RestController
@RequestMapping("/fxtrading/orders")
public class OrderController {

    @Autowired
    OrderService orderService;

    @Autowired
    OrderResourceAssembler assembler;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Collection<OrderResource>> getAllOrders(@RequestParam String query) {
	List<Order> orders = new ArrayList<Order>();
	if (query.equals("all"))
	    orders = orderService.findAll();
	else if (query.equals("matched"))
	    orders = orderService.getMatchedOrders();
	else if (query.equals("unmatched"))
	    orders = orderService.getUnMatchedOrders();
	return new ResponseEntity<>(assembler.toResourceCollection(orders), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseEntity<OrderResource> getOrderById(@PathVariable(value = "id") Long id) {
	Order order = orderService.findOrderById(id).orElseThrow(() -> new ResourceNotFoundException("order", "id", id));
	return new ResponseEntity<>(assembler.toResource(order), HttpStatus.OK);
    }


    @RequestMapping(method = RequestMethod.GET, value = "/pairs/{id}")
    public ResponseEntity<Collection<OrderResource>> getPairedOrdersById(@PathVariable(value = "id") Long id) {
	List<Order> orders = new ArrayList<Order>();
	orders = orderService.findPairedOrdersById(id);
	return new ResponseEntity<>(assembler.toResourceCollection(orders), HttpStatus.OK);
    }

    
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<OrderResource> createOrder(@RequestBody Order order) {
	Order insertedOrder = orderService.insertOrder(order);
	return new ResponseEntity<>(assembler.toResource(insertedOrder), HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    public ResponseEntity<OrderResource> modifyOrder(@RequestBody Order order, @PathVariable(value = "id") Long id) {
	order.setId(id);
	orderService.modify(order);
	return new ResponseEntity<>(assembler.toResource(order), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public ResponseEntity<OrderResource> cacnelOrder(@PathVariable(value = "id") Long id) {
	return new ResponseEntity<>(assembler.toResource(orderService.cancelOrder(id)), HttpStatus.ACCEPTED);
    }

}
