package com.worldfirst.demofxtrading.model;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Order implements Identifiable {

    private long id;
    private String orderType;
    private String currency;
    private long userId;
    private BigDecimal amount;
    private boolean isMatched;

    public Order() {

    }

    public Order(long orderId, long userId, String currency, String orderType, String amount) {
	this.id = orderId;
	this.userId = userId;
	this.currency = currency;
	this.orderType = orderType;
	this.amount = new BigDecimal(amount);
    }

    public Order(long orderId, long userId, String currency, String orderType, String amount, boolean isMatched) {
	this.id = orderId;
	this.userId = userId;
	this.currency = currency;
	this.orderType = orderType;
	this.amount = new BigDecimal(amount);
	this.isMatched=isMatched;
    }

    @Override
    public Long getId() {
	return id;
    }

    @Override
    public void setId(Long id) {
	this.id = id;
    }

    public String getOrderType() {
	return orderType;
    }

    public void setOrderType(String orderType) {
	this.orderType = orderType;
    }

    public String getCurrency() {
	return currency;
    }

    public void setCurrency(String currency) {
	this.currency = currency;
    }

    public long getUserId() {
	return userId;
    }

    public void setUserId(long userId) {
	this.userId = userId;
    }

    public BigDecimal getAmount() {
	return amount;
    }

    public void setAmount(BigDecimal amount) {
	this.amount = amount;
    }

    public boolean isMatched() {
	return isMatched;
    }

    public void setMatched(boolean isMatched) {
	this.isMatched = isMatched;
    }

    public String toString() {
	return "[OrderId:" + this.id + ", Userid:" + this.userId + ", Currency:" + this.currency + ", Amount:"
		+ this.amount + ", orderType:" + this.orderType + ", isMatched:" + this.isMatched + "]";

    }

}
