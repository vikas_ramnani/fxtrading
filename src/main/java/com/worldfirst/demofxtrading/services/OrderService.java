package com.worldfirst.demofxtrading.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import org.springframework.stereotype.Component;

import com.worldfirst.demofxtrading.database.ExchangeProperty;
import com.worldfirst.demofxtrading.model.Order;
import com.worldfirst.demofxtrading.repositories.OrderRepository;

@Component
public class OrderService {

    private static final Logger LOGGER = Logger.getLogger(OrderService.class.getName());

    // TO DO : why autowired not working
    OrderRepository orderRepository = new OrderRepository();;

    public List<Order> findAll() {
	return orderRepository.findAll();
    }

    public Optional<Order> findOrderById(Long id) {
	return orderRepository.findOrderById(id);
    }

    public Order insertOrder(Order order) {
	LOGGER.info("inserting the order : " + order);
	order.setMatched(false);
	LOGGER.info("The field 'isMatched' is set to false initially");
	
	// Check if the order is Matched according to certain criterias
	Order matchingOrder = findMatchingOrderInList(order, getUnMatchedOrders());
	if (matchingOrder != null) {
	    LOGGER.info(order+" is matched with "+matchingOrder);
	    LOGGER.info("We will set the isMatched to True on both orders");
	    matchingOrder.setMatched(true);
	    order.setMatched(true);
	}
	return orderRepository.insertOrder(order);
    }

   
    public Order modify(Order order) {
	//To be able to modify the Order we should go through Delete And Insert to be able to pass by Matching Correctly.
	cancelOrder(order.getId());
	return insertOrder(order);
    }

    public Order cancelOrder(Long id) {
	// Before deleting the Order, we have to find out if there was any matched order in that case we have 'unmatch' that other order
	Order matchedOrder = findMatchingOrderInList(findOrderById(id).get(),getMatchedOrders());
	if (matchedOrder != null) {
	    matchedOrder.setMatched(false);
	    orderRepository.modify(matchedOrder);
	}
	return orderRepository.cancelOrder(id);
    }

    public List<Order> getUnMatchedOrders() {
	return orderRepository.getUnMatchedOrders();
    }

    public String getExchangeRate(String key) {
	return ExchangeProperty.getInstance().getProperty(key);
    }

    public List<Order> getMatchedOrders() {
	return orderRepository.getMatchedOrders();
    }

    public List<Order> findPairedOrdersById(Long id) {
	List<Order> orders = new ArrayList<Order>();
	orders.add(findMatchingOrderInList(findOrderById(id).get(),getMatchedOrders()));
	orders.add(findOrderById(id).get());
	return orders;
    }

          
    private Order findMatchingOrderInList(Order order, List<Order> listOrders) {	
   	Order matchingOrder = null;
   	for (Order eachOrder : listOrders) {
   	    if (isTwoOrdersMatched(eachOrder, order)) {
   		eachOrder.setMatched(true);
   		order.setMatched(true);
   		matchingOrder = eachOrder;
   		break;
   	    }
   	}
   	return matchingOrder;
       }

    private boolean isTwoOrdersMatched(Order order1, Order order2) {
   	if (order1.getOrderType().equals(order2.getOrderType()) && !order1.getCurrency().equals(order2.getCurrency())
   		&& isAmountMatched(order1, order2))
   	    return true;
   	else
   	    return false;
       }
    
    private boolean isAmountMatched(Order order1, Order order2) {
   	if (order1.getCurrency().equals(order2.getCurrency())) {
   	    // in case we are here with same currencies (which we should not as per the
   	    // previous conditions)
   	    return false;
   	}
   	// we can have pair of GBP-USD and USD-GBP in the property file. Both should be
   	// checked.
   	String exchangeRate = getExchangeRate(order1.getCurrency() + "-" + order2.getCurrency());
   	String exhangeRateInverse = getExchangeRate(order2.getCurrency() + "-" + order1.getCurrency());

   	if (exchangeRate != null) {
   	    // this means the exchange rate configuration exists between these two currency
   	    if (order1.getAmount().multiply(new BigDecimal(exchangeRate)).compareTo(order2.getAmount()) == 0) {
   		return true;
   	    }
   	} else if (exhangeRateInverse != null) {
   	    if (order2.getAmount().multiply(new BigDecimal(exhangeRateInverse)).compareTo(order1.getAmount()) == 0) {
   		return true;
   	    }
   	}
   	return false;
       }


}
